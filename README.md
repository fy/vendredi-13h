Ce projet gitlab héberge le groupe de lecture du GDR TAL
--------------------------------------------------------

Le mode de fonctionnement est le suivant :
1. Chacun peut proposer des articles à la lecture
2. On peut ensuite voter pour les articles à lire
3. À partir des votes et en assurant un équilibre thématique, deux ou trois articles sont sélectionnés
4. Les participants doivent avoir lu les articles avant la réunion
5. Lors de la réunion un "parain" présente rapidement (5 minutes) le contenu de l'article, puis tout le monde discute
6. En fin de réunion, on sélectionne les articles et parains pour la prochaine itération
7. Les issues associées à un article déjà lu sont fermées

**Comment proposer un article ?**
- Il faut se connecter à gitlab et créer une "issue". Cette dernière devra contenir les titre, auteurs, résumé et un lien vers le pdf. Les issues ne doivent pas être utilisées pour autre chose.

**Comment voter pour un article ?**
- Cliquez sur le pouce (👍) associé à l'issue

**Prochains groupes de lecture :**
- 24 janvier 2020
- 7 février 2020
- 21 février 2020
- 6 mars 2020
- 27 mars 2020
- 24 avril 2020